#  Загрузка видео
Для загрузки видео предусмотрено два формата: youtube и html5 видео

    <div class="video-list__item" data-format="video" data-src="assets/video/block-13.mp4">
        <p class="video-list__item-title">1. Что такое блокчейн?</p>
        <img src="assets/img/icons/youtube-transparent.svg" alt="">
    </div>


    <div class="video-list__item hidden-on-mob" data-format="frame" data-src='<iframe width="100%" height="100%" src="https://www.youtube.com/embed/H7sVilby7Rc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'>
        <p class="video-list__item-title" >6. Фьючерсы</p>
        <img src="assets/img/icons/youtube-transparent.svg" alt="">
    </div>

